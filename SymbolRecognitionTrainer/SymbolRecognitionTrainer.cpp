#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"

#include "FeatureExtraction.h"
#include "MomentsHelper.h"
#include "MyMoments.h"

#include "../FeatureExtractionLib/FeatureExtraction.h"
#include "Visualisation.h"

#include <time.h>
#include <windows.h>
#include <fstream>

#define VAL_FILENAME "value.txt"

using namespace cv;
using namespace std;
using namespace fe;

std::map<int, std::string> val;

MomentsHelper a;
MyMoments rec;
int maxx, diametr;

std::shared_ptr<fe::PolynomialManager> polim;
std::shared_ptr<fe::IBlobProcessor> bld;

void generateData()
{
	bool tmp = a.DistributeData("..\\Data\\labeled_data", "..\\Data\\ground_data", "..\\Data\\test_data", 50);
	std::vector<std::string> paths;

	 //���������� ����� ����������� ���������
	std::map<std::string, std::vector<fe::ComplexMoments>> MomentsGr, MomentsT;
	MomentsHelper::GenerateMoments("..\\Data\\ground_data\\", bld, polim, MomentsGr);		
	MomentsHelper::SaveMoments("MomentsGr.txt", MomentsGr);		
	
	MomentsHelper::GenerateMoments("..\\Data\\test_data\\", bld, polim, MomentsT);		
	MomentsHelper::SaveMoments("MomentsT.txt", MomentsT);
	std::cout << "===Gen data!===" << endl;
}

void trainNetwork()
{
	bool tmp;
	map<string, vector<ComplexMoments>> DataMoments;
	std::vector<int> l;
	l.push_back(50);
	MomentsHelper::ReadMoments("MomentsGr.txt", DataMoments);
	tmp = rec.Train(DataMoments, l, 10000, 0.01f, 0.1f);
	rec.Save("Neuro.txt");
	if (tmp)
		std::cout << "===Train network was successful !==="  << endl;
	else
		std::cout << "===Train network was not successful !==="  << endl;

}

void precisionTest()
{

	rec.Read("Neuro.txt");
	std::map<std::string, std::vector<fe::ComplexMoments>> DataMoments;
	std::vector<std::string> paths;
	MomentsHelper::ReadMoments("MomentsT.txt", DataMoments);
	std::cout << rec.PrecisionTest(DataMoments) << " ";
	std::cout << endl;
	std::cout << "===Precision test!===" << endl;
}

void recognizeImage()
{
	
	rec.Read("Neuro.txt");
		string name_pic,path;
		
		cout << "Enter picture title (test1,test2,test3)" << endl;
		cin >> name_pic;		
		
		cv::Mat image = cv::imread("..\\SymbolRecognitionTrainer\\" + name_pic + ".png", CV_8UC1);
	
		if (image.empty()) { cout << "ERROR: Empty image" << endl; }
		
			imshow("Image to recognize", image);
			vector<Mat> blobs = bld->DetectBlobs(image);
			vector<Mat> nblobs = bld->NormalizeBlobs(blobs, polim->GetBasis()[0][0].first.cols);
			vector<ComplexMoments> res;
			for (int i = 0; i < nblobs.size(); i++)
			{
				res.push_back(polim->Decompose(nblobs[i]));
				string num = rec.Recognize(res.back());
				cout << " Recognized number: " << num << endl;
				ShowBlobDecomposition( "Recognized number:" + num, nblobs[i], polim->Recovery(res.back()));
				waitKey();
			}			
			
			destroyAllWindows();
		
	
	std::cout << "===Recognize single image!===" << endl;
}

int main(int argc, char** argv)
{
	srand(time(0));
	bld = fe::CreateBlobProcessor();
	polim = fe::CreatePolynomialManager();
	cout << "Max polinom:"; 	cin >> maxx;	cout << endl;
	cout << "Diametr:";	cin >> diametr;	cout << endl;

	polim->InitBasis(maxx, diametr);
	string key;
	do 
	{
		std::cout << "===Enter next values to do something:===" << endl;
		std::cout << "  '1' - to generate data." << endl;
		std::cout << "  '2' - to train network." << endl;
		std::cout << "  '3' - to check recognizing precision." << endl;
		std::cout << "  '4' - to recognize single image." << endl;
		std::cout << "  'exit' - to close the application." << endl;
		cin >> key;
		std::cout << endl;
		if (key == "1") {
			generateData();
		}
		else if (key == "2") {
			trainNetwork();
		}
		else if (key == "3") {
			precisionTest();
		}
		else if (key == "4") {
			recognizeImage();
		}
		std::cout << endl;
	} while (key != "exit");
	return 0;
}