#include "LezhandrPolynomialManager.h"
#include "RadialFunctions.h"

#define M_PI 3.1415926535897932


LezhandrPolynomialManager::LezhandrPolynomialManager()
{
}


LezhandrPolynomialManager::~LezhandrPolynomialManager()
{
}

fe::ComplexMoments LezhandrPolynomialManager::Decompose(cv::Mat blob)
{
	fe::ComplexMoments decomposition;

	decomposition.re = cv::Mat::zeros(polynomials.size(), polynomials[0].size(), CV_64FC1);
	decomposition.im = cv::Mat::zeros(polynomials.size(), polynomials[0].size(), CV_64FC1);
	decomposition.abs = cv::Mat::zeros(polynomials.size(), polynomials[0].size(), CV_64FC1);
	decomposition.phase = cv::Mat::zeros(polynomials.size(), polynomials[0].size(), CV_64FC1);
	double norma = polynomials.size() * polynomials[0].size();

	cv::Mat other;
	blob.convertTo(other, CV_64FC1);

	size_t basis_idx = 0;
	for (size_t i = 0; i < polynomials.size(); i++)
	{
		for (size_t j = 0; j < polynomials[0].size(); j++)
		{
			double temp = other.dot(polynomials[i][j].first);
			double tabs = polynomials[i][j].first.dot(polynomials[i][j].first);
			decomposition.re.at<double>(i, j) = (abs(temp) > 1e-20 ? (temp / tabs) / norma : 0.0);

			temp = other.dot(polynomials[i][j].second);
			tabs = polynomials[i][j].second.dot(polynomials[i][j].second);
			decomposition.im.at<double>(i, j) = (abs(temp) > 1e-20 ? (temp / tabs) / norma : 0.0);

			decomposition.abs.at<double>(i, j) = sqrt(decomposition.re.at<double>(i, j) * decomposition.re.at<double>(i, j) + decomposition.im.at<double>(i, j) *decomposition.im.at<double>(i, j));
			decomposition.phase.at<double>(i, j) = atan2(decomposition.im.at<double>(i, j), decomposition.re.at<double>(i, j));

		}
	}
	return decomposition;
}

cv::Mat LezhandrPolynomialManager::Recovery(fe::ComplexMoments& decomposition)
{
	cv::Mat recovery = cv::Mat(this->polynomials[0][0].first.rows, this->polynomials[0][0].first.cols, CV_64FC1);
	recovery.setTo(cv::Scalar(0));

	for (size_t i = 0; i < this->polynomials.size(); ++i)
	{
		for (size_t j = 0; j < this->polynomials[i].size(); ++j)
		{
			recovery += this->polynomials[i][j].first * decomposition.re.at<double>(i, j)
				+ this->polynomials[i][j].second * decomposition.im.at<double>(i, j);

		}
	}
	return recovery;
}

void LezhandrPolynomialManager::InitBasis(int n_max, int diameter)
{
	double delta = 2. / diameter;
	this->polynomials.resize(n_max + 1);
	for (size_t n = 0; n <= n_max; ++n)
	{
		this->polynomials[n].resize(n_max + 1);
		for (size_t i = 0; i <= n_max; ++i)
		{
			this->polynomials[n][i] = std::make_pair(cv::Mat(diameter, diameter, CV_64FC1), cv::Mat(diameter, diameter, CV_64FC1));
			this->polynomials[n][i].first.setTo(cv::Scalar(0));
			this->polynomials[n][i].second.setTo(cv::Scalar(0));
		}
		for (size_t r = 0; r < diameter / 2; ++r)
		{
			double radial = rf::RadialFunctions::ShiftedLegendre(r * delta, n);
			size_t rot_count = (size_t)(2 * M_PI * r * delta * diameter) * 2;
			for (size_t th = 0; th < rot_count; ++th)
			{
				double theta = 2 * th * M_PI / rot_count;
				for (size_t i = 0; i <= n_max; ++i)
				{
					double sine = std::sin(theta * i) * radial;
					double cosine = std::cos(theta * i) * radial;
					double& color_re = this->polynomials[n][i].first.at<double>(r * std::cos(theta) + diameter / 2, r * std::sin(theta) + diameter / 2);
					color_re = cosine;
					double& color_im = this->polynomials[n][i].second.at<double>(r * std::cos(theta) + diameter / 2, r * std::sin(theta) + diameter / 2);
					color_im = sine;
				}
			}
		}
	}
	
}

std::string LezhandrPolynomialManager::GetType()
{
	return "Graf ";
}
