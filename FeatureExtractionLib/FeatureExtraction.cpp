#define FEATURE_DLL_EXPORTS

#include "FeatureExtraction.h"

#include "LezhandrPolynomialManager.h"
#include "SimpleBlobProcessor.h"


std::string fe::GetTestString()
{
	return "ANN by Chugrina Alina";
}

/**
 * ������� ���������� ������� ��������.
 * @return ���������� ������� ��������.
 */
std::shared_ptr<fe::IBlobProcessor> fe::CreateBlobProcessor() 
{
	return std::shared_ptr<fe::IBlobProcessor>(new SimpleBlobProcessor());
}

/**
 * ������� ������, ������������� �� ������ � ����������.
 * @return ������, ������������ �� ������ � ����������.
 */
std::shared_ptr<fe::PolynomialManager> fe::CreatePolynomialManager()
{
	return std::shared_ptr<fe::PolynomialManager>(new LezhandrPolynomialManager());
}

