#pragma once
#include "PolynomialManager.h"

class LezhandrPolynomialManager :	public fe::PolynomialManager
	{
	public:
		LezhandrPolynomialManager();
		~LezhandrPolynomialManager();
		fe::ComplexMoments Decompose(cv::Mat blob);

		/**
		 * ������������ �������� �� ����������.
		 * @param decomposition - ���������� �������� � ���.
		 * @return ��������������� �����������, ����� ��� CV_64FC1.
		 */
		cv::Mat Recovery(fe::ComplexMoments& decomposition);

		/**
		 * ������������������� ����� ������������� ��������� ~ exp(jm*fi).
		 * @param n_max - ������������ ���������� ������� ���������.
		 * @param diameter - ������� ����������, �� ������� ����� ������������� ��������, �������.
		 */
		void InitBasis(int n_max, int diameter);

		/**
		 * �������� �������� ������� ��� ������ � ����������.
		 * @return - �������� �������.
		 */
		std::string GetType();
	};

