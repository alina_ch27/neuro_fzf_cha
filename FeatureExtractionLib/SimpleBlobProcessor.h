#pragma once
#include "IBlobProcessor.h"
class SimpleBlobProcessor :	public fe::IBlobProcessor
	{
	public:
		SimpleBlobProcessor();
		~SimpleBlobProcessor();
		std::string GetType();
		std::vector<cv::Mat> DetectBlobs(cv::Mat image);

		/**
		 * �������� ������ ������� �������� � ������� ��������.
		 * @param blobs - ������� �������, ������ ������������ ����� ����� ������� �� ������ ����, ��� CV_8UC1.
		 * @param normilized_blobs - ����� ��� ������ ������� �������� ������� �������.
		 *               ������� ������� ����� ������ ������������ ����� ����� ����� �� ������ ����, ��� CV_8UC1.
		 * @param side - ������� �������� �� ������� ����� ���������� ��������������� ������� �������.
		 */
		std::vector<cv::Mat> NormalizeBlobs(
			std::vector<cv::Mat>& blobs,
			int side
		);
	};

