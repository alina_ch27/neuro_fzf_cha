#include <algorithm>
#include <iostream>
#include "GeneticAlgorithm.h"
using  namespace std;
using namespace ga;

int main()
{
	cout << "Population size: "; int population_size = 20; cin >> population_size;
	if (population_size > 0)
	{
		vector<size_t> config;
		// Neural network configuration:
		config.push_back(2); config.push_back(4); config.push_back(1);
		GeneticAlgorithm GAl(config, population_size);

		cout << "Percent of survivors:   "; float unchange_perc = 0.5;  cin >> unchange_perc;
		cout << "Percent of descendants: "; float crossover_perc = 0.2; cin >> crossover_perc;
		cout << "Percent of permutation: "; float mutation_perc = 0.05; cin >> mutation_perc;
		unchange_perc *= 0.01; crossover_perc *= 0.01; mutation_perc *= 0.01;
		cout << "Maximum number of epochs to pass: "; int lineage = 4;  cin >> lineage;
		cout << endl << "Evolution start!" << endl;
		while ((lineage > 0) && (population_size > 1))
		{
			GAl.epoch->EpochBattle();
			cout << endl << "Epoch battle scoreboard:" << endl;
			for each (auto unit in GAl.epoch->population) { cout << unit.first << '\t'; }

			GAl.Selection(unchange_perc, mutation_perc, crossover_perc);
			cout << endl << "Selection results: ";
			cout << (int)(population_size * unchange_perc) << " survived, ";
			cout << (int)(population_size * mutation_perc) << " mutated, ";
			cout << (int)(population_size * crossover_perc) << " newborn, ";
			cout << (population_size - GAl.epoch->population.size()) << " extinct" << endl;

			population_size = GAl.epoch->population.size();
			lineage--;
		}

		if (lineage > 0) { cout << "Extinction reached! " << lineage << " more epochs remained." << endl; }

		GAl.epoch->EpochBattle();
		sort(GAl.epoch->population.begin(), GAl.epoch->population.end(),
			[](pair<int, pIIndividual> a, pair<int, pIIndividual> b)
		{ return a.first > b.first; });

		vector<vector<float>> inputs, outputs;
		ANN::LoadData("xor.data", inputs, outputs);
		cout << endl << "Total evolution results:" << endl << "Score\tPredictions";
		for (int i = 0; i < population_size; i++)
		{
			cout << endl << GAl.epoch->population[i].first;
			float eps = 0.0f;
			for (int k = 0; k < inputs.size(); k++)
			{
				auto prediction = GAl.epoch->population[i].second->MakeDecision(inputs[k]);
				for (int l = 0; l < outputs[k].size(); l++) {
					cout << '\t' << prediction[l];
					eps += (outputs[k][l] - prediction[l]) * (outputs[k][l] - prediction[l]);
				}
			}
			cout << "\te=" << eps << endl;
		}
	}
	cout << endl; system("pause");
	return 0;
}