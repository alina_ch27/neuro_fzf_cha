#define ANNDLL_EXPORTS
#include "NeuralRealisation.h"
using namespace std;

ANN::NeuralRealisation::NeuralRealisation(
	std::vector<size_t> & configuration,
	ANN::ANeuralNetwork::ActivationType activation_type,
	float scale
	)
{
	this->configuration = configuration;
	this->activation_type = activation_type;
	this->scale = scale;
}


std::shared_ptr<ANN::ANeuralNetwork> ANN::CreateNeuralNetwork(
	std::vector<size_t> & configuration,
	ANN::ANeuralNetwork::ActivationType activation_type,
	float scale
	)
{
	return std::make_shared<ANN::NeuralRealisation>(configuration, activation_type, scale);
}


std::string ANN::NeuralRealisation::GetType()
{
	return "Neural network by Chugrina Alina";
}


std::vector<float> ANN::NeuralRealisation::Predict(std::vector<float> & input)
{
	std::vector<float> result, buffer = input;
	for (size_t layer_idx = 0; layer_idx < (configuration.size() - 1); layer_idx++)
	{
		result.resize(configuration[layer_idx + 1]);
		for (size_t to_idx = 0; to_idx < configuration[layer_idx + 1]; to_idx++)
		{
			result[to_idx] = 0.0f;
			for (size_t from_idx = 0; from_idx < configuration[layer_idx]; from_idx++)
				result[to_idx] += buffer[from_idx] * weights[layer_idx][from_idx][to_idx];
			result[to_idx] = Activation(result[to_idx]);
		}
		buffer = result;
	}
	return result;
}


float ANN::BackPropTraining(
	std::shared_ptr<ANN::ANeuralNetwork> ann,
	std::vector<std::vector<float>> & inputs,
	std::vector<std::vector<float>> & outputs,
	int maxIters,
	float eps,
	float speed,
	bool std_dump
	)
{
	float err = 0.0;
	for (size_t j = 0; j < inputs.size(); j++)
	{
		std::vector<float> result = ann->Predict(inputs[j]);
		for (size_t i = 0; i < result.size(); i++)
			err += (result[i] - outputs[j][i]) * (result[i] - outputs[j][i]);
	}
	err *= 0.5f;

	size_t Iters = 0;
	do {
		err = 0.0;
		for (size_t j = 0; j < inputs.size(); j++)
			err += BackPropTrainingIteration(ann, inputs[j], outputs[j], speed);
		err /= (float)(inputs.size());

		Iters++;
		if (std_dump && (Iters % 100 == 0)) printf("\t%i: error %.4f\n", Iters, err);

		if (err < eps) break;
	} while (Iters < maxIters);

	printf("\t%i: error %.4f\n", Iters, err);
	ann->is_trained = true;
	return err;
}


float ANN::BackPropTrainingIteration(
	std::shared_ptr<ANN::ANeuralNetwork> ann,
	const std::vector<float>& input,
	const std::vector<float>& output,
	float speed
	)
{
	// ��������� ������� � �������� �������� ���� ��������:
	std::vector<std::vector<float>> buffers = std::vector<std::vector<float>>(ann->configuration.size());
	buffers[0] = input;
	for (size_t layer_idx = 1; layer_idx < buffers.size(); layer_idx++)
	{
		buffers[layer_idx] = std::vector<float>(ann->configuration[layer_idx]);
		for (size_t to_idx = 0; to_idx < buffers[layer_idx].size(); to_idx++)
		{
			buffers[layer_idx][to_idx] = 0.0f;
			for (size_t from_idx = 0; from_idx < buffers[layer_idx - 1].size(); from_idx++)
				buffers[layer_idx][to_idx] += buffers[layer_idx - 1][from_idx] * ann->weights[layer_idx - 1][from_idx][to_idx];
			buffers[layer_idx][to_idx] = ann->Activation(buffers[layer_idx][to_idx]);
		}
	}

	// ������������� ����� �������� ��������� ����:
	std::vector<float> deltas = std::vector<float>(ann->configuration.back());
	size_t corr_idx = ann->weights.size() - 1;
	for (size_t to_idx = 0; to_idx < ann->configuration[corr_idx + 1]; to_idx++)
	{
		deltas[to_idx] = (buffers[corr_idx + 1][to_idx] - output[to_idx]);
		deltas[to_idx] *= ann->ActivationDerivative(buffers[corr_idx + 1][to_idx]);

		for (size_t from_idx = 0; from_idx < ann->configuration[corr_idx]; from_idx++)
			ann->weights[corr_idx][from_idx][to_idx] -= speed * deltas[to_idx] * buffers[corr_idx][from_idx];
	}

	// ������������� ����� �������� ���������� ����:
	while (corr_idx > 0)
	{
		corr_idx--;
		std::vector<float> deltas_new = std::vector<float>(ann->configuration[corr_idx + 1]);
		for (size_t to_idx = 0; to_idx < ann->configuration[corr_idx + 1]; to_idx++)
		{
			deltas_new[to_idx] = 0.0;
			for (size_t k = 0; k < ann->configuration[corr_idx + 2]; k++)
				deltas_new[to_idx] += deltas[k] * ann->weights[corr_idx + 1][to_idx][k];
			deltas_new[to_idx] *= ann->ActivationDerivative(buffers[corr_idx + 1][to_idx]);

			for (size_t from_idx = 0; from_idx < ann->configuration[corr_idx]; from_idx++)
				ann->weights[corr_idx][from_idx][to_idx] -= speed * deltas_new[to_idx] * buffers[corr_idx][from_idx];
		}
		deltas = deltas_new;
	}

	// ��������� ������ ������ ��������� ����:
	buffers[0] = input;
	for (size_t layer_idx = 1; layer_idx < buffers.size(); layer_idx++)
	{
		for (size_t to_idx = 0; to_idx < buffers[layer_idx].size(); to_idx++)
		{
			buffers[layer_idx][to_idx] = 0.0f;
			for (size_t from_idx = 0; from_idx < buffers[layer_idx - 1].size(); from_idx++)
				buffers[layer_idx][to_idx] += buffers[layer_idx - 1][from_idx] * ann->weights[layer_idx - 1][from_idx][to_idx];
			buffers[layer_idx][to_idx] = ann->Activation(buffers[layer_idx][to_idx]);
		}
	}
	// ��������� ����������� ������ � ��������� �������:
	float err = 0.0f;
	for (size_t i = 0; i < buffers.back().size(); i++)
		err += (buffers.back()[i] - output[i]) * (buffers.back()[i] - output[i]);
	return 0.5f * err;
}

