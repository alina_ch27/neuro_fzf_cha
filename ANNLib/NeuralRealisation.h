#pragma once
#include "ANN.h"
#include <iostream>


namespace ANN
{
	class NeuralRealisation : public ANN::ANeuralNetwork
	{
	public:
		ANNDLL_API NeuralRealisation(
			std::vector<size_t> & configuration,
			ANN::ANeuralNetwork::ActivationType activation_type,
			float scale
			);

		ANNDLL_API virtual std::string GetType() override;
		ANNDLL_API virtual std::vector<float> Predict(std::vector<float> & input) override;
	};
	
}

