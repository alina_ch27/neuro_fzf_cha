#include <iostream>
#include <NeuralRealisation.h>
using namespace std;
using namespace ANN;

int main()
{
	printf((ANN::GetTestString() + "\n").c_str());

	std::shared_ptr<ANN::ANeuralNetwork> pANN = ANN::CreateNeuralNetwork();
	if (pANN->Load("..\\Debug\\xor.ann"))
	{
		printf((pANN->GetType() + "\n").c_str());
		std::vector<std::vector<float>> inputs, outputs;
		ANN::LoadData("..\\Debug\\xor_prime.data", inputs, outputs);
		for (int i = 0; i < inputs.size(); i++)
		{
			std::vector<float> result = pANN->Predict(inputs[i]);
			for (int j = 0; j < inputs[i].size(); j++)
				printf(" %.2f", inputs[i][j]);
			printf(":");
			for (int j = 0; j < result.size(); j++)
				printf(" %.3f", result[j]);
			printf("\n\n");
		}
	}

	system("pause");
	return 0;
}